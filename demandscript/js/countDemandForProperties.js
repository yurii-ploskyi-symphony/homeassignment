const limit = 50;
const totalAirBnbApiLimit = 1000;
const clientId = '3092nxybyb0otqw18e8nh5nty';
const airBnbBaseUrl = 'https://api.airbnb.com/v2';
const searchResultsUrl = `${airBnbBaseUrl}/search_results?client_id=${clientId}&_limit=${limit}`;
const similarListingsUrl = `${airBnbBaseUrl}/similar_listings?_format=for_similar_listings_with_prices&client_id=${clientId}&_limit=50`;

// GET http request for url
const get = (url) => new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.open('GET', url);
    request.onload = () => {
        if (request.status === 200) {
            resolve(request.response);
        }
        else {
            reject(Error(request.statusText));
        }
    };
    request.onerror = () => reject(Error("Network Error"));
    request.send();
});

// returns a single Promise that resolves when all of
// the promises in the iterable argument have resolved
// or when the iterable argument contains no promises.
// It rejects with the reason of the first promise that rejects.
const convertArrayOfPromisesToOnePromise = arrayOfPromises => Promise.all(arrayOfPromises);

// Converts array of arrays into one long array
const flatten = arrayOfArrays => [].concat.apply([], arrayOfArrays);

// Remove duplicates values from array. Two elements considered
// as equal if both have the same id.
// Since removing duplicated values takes too long time for big arrays
// array divides on small arrays before filtering and method runs filtering
// task for each small array in parallel asynchronously. In the end all results are merged
// in one array and returned.
const removeDuplicatesByUniqueId = array => {
    const split = (array, lengthOfChunk) => {
        const result = [];
        while (array.length) {
            result.push(array.splice(0, lengthOfChunk));
        }
        return result;
    };
    const removeDuplicatesFromArrays = (arrays, uniqueField) => arrays.map(array => removeDuplicatesPromise(array, uniqueField));
    const removeDuplicatesPromise = (array, uniqueField) => new Promise((resolve) => resolve(removeDuplicatesSynchronously(array, uniqueField)));
    const removeDuplicates = (arrays, uniqueField) => convertArrayOfPromisesToOnePromise(removeDuplicatesFromArrays(arrays, uniqueField));
    const mergeFilteredArrays = (uniqueElementsPromises, uniqueField) => removeDuplicatesSynchronously(flatten(uniqueElementsPromises), uniqueField);
    const removeDuplicatesInParallel = (array, uniqueField) => {
        const smallArrays = split(array, 1000);
        return removeDuplicates(smallArrays, uniqueField).then(uniqueElementsPromises => mergeFilteredArrays(uniqueElementsPromises, uniqueField));
    };
    const removeDuplicatesSynchronously = (array, uniqueField) => {
        return array.filter((obj, pos, arr) => {
            return arr.map(mapObj => mapObj[uniqueField]).indexOf(obj[uniqueField]) === pos;
        });
    };
    return removeDuplicatesInParallel(array, 'id');
};

const convertRequestResultsToListings = requestResults => requestResults.map(fetchListing);
const fetchListing = object => object.listing;

// Requests all pages of listings from airBnb. Returns array of promises.
const getSearchResultsPromises = city => {
    const getSearchResultsUrl = (location, offset) => `${searchResultsUrl}&location=${location}&_offset=${offset}`;
    const getListingPromises = (city, offset) => get(getSearchResultsUrl(city, offset)).then(response => {
        const searchResults = JSON.parse(response).search_results;
        return convertRequestResultsToListings(searchResults);
    });
    const promises = [];
    for (let offset = 0; offset < totalAirBnbApiLimit; offset += limit) {
        promises.push(getListingPromises(city, offset));
    }
    return promises;
};

// Generates text file, creates link for that file and
// initiates download action from browser
const createAndDownloadFile = text => {
    let textFile = null;
    const makeTextFile = text => {
        const data = new Blob([text], {type: 'text/plain'});
        // If we are replacing a previously generated file we need to
        // manually revoke the object URL to avoid memory leaks.
        if (textFile) {
            window.URL.revokeObjectURL(textFile);
        }
        textFile = window.URL.createObjectURL(data);
        return textFile;
    };
    const createLink = (text) => {
        const link = document.createElement('a');
        link.setAttribute('download', 'demands.json');
        link.href = makeTextFile(text);
        return link;
    };
    const download = (link) => {
        // wait for the link to be added to the document
        window.requestAnimationFrame(() => {
            const event = new MouseEvent('click');
            link.dispatchEvent(event);
        });
    };
    const link = createLink(text);
    download(link);
};

const convertListingsToDemands = listings => {
    const countDemand = (lat, lng, starRating) => (starRating * 5 + 0.3 * lat + 0.6 * lng) / 100;
    const convertListingToDemand = listing => ({
        'lat': listing.lat,
        'lng': listing.lng,
        'demand': countDemand(listing.lat, listing.lng, listing.star_rating)
    });
    return listings.map(convertListingToDemand);
};

const handleSimilarListingsResponsePromises = (searchListings, similarListingsResponsePromises) => {
    const similarListings = convertRequestResultsToListings(similarListingsResponsePromises);
    removeDuplicatesByUniqueId(searchListings.concat(similarListings)).then(mergedUniqueListings => {
        const demands = convertListingsToDemands(mergedUniqueListings);
        console.log(demands);
        createAndDownloadFile(JSON.stringify(demands, null, 4));
    });
};

// All staff for fetching and handling similar listings for listing array
const getSimilarListingPromise = listingsPromise => {
    const getSimilarListingsPromises = listings => listings.map(getSimilarListingsPromise);
    const getSimilarListingsPromise = listing => getSimilarListingsPromiseById(listing.id);
    const fetchSimilarListingsFromResponse = response => JSON.parse(response).similar_listings;
    const requestSimilarListings = listingId => get(getSimilarListingsUrl(listingId));
    const getSimilarListingsPromiseById = listingId => requestSimilarListings(listingId).then(fetchSimilarListingsFromResponse);
    const getSimilarListingsUrl = listingId => `${similarListingsUrl}&listing_id=${listingId}`;
    return convertArrayOfPromisesToOnePromise(getSimilarListingsPromises(listingsPromise))
};

// Fetches similar listings for each listing. After, merges similar listings with search listings
const fetchSimilarListingsAndMerge = listingsPromise =>
    getSimilarListingPromise(listingsPromise)
        .then(flatten)
        .then(similarListingsResponsePromises => handleSimilarListingsResponsePromises(listingsPromise, similarListingsResponsePromises));

const handleListingPromises = listingsPromises => {
    const listingsPromise = flatten(listingsPromises);
    removeDuplicatesByUniqueId(listingsPromise).then(uniqueListings => {
        fetchSimilarListingsAndMerge(uniqueListings);
    });

};

// Main function. Start point of script
const countAndSaveToFileDemandForProperties = city => {
    const handleSearchResultsPromises = searchResultsPromises => convertArrayOfPromisesToOnePromise(searchResultsPromises).then(flatten).then(handleListingPromises);
    const searchResultsPromises = getSearchResultsPromises(city);
    handleSearchResultsPromises(searchResultsPromises);
};
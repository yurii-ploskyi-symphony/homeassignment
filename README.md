# Properties demands generator

# Prerequisites
- Have [Node](https://nodejs.org/en/) installed
- Have [NPM](https://www.npmjs.com/) installed

## Installation

	1) Navigate to project root
	2) Run npm install
	3) Run npm start
	4) Navigate to http://localhost:8080/heatmap/index.html
	5) Navigate to http://localhost:8080/demandscript/index.html

## Files and folders description
|File or folder  |description                          |
|----------------|-------------------------------|
|demandscript    |`Folder with all staff for generating demans`            |
|demandscript/index.html          |`Simple html page with script usage`            |
|demandscript/js/main.js          |`Handler of start button`|
|demandscript/js/countDemandForProperties.js          |`Main file. Script for generating and downloading demands`|
|heatmap          |`Folder with all staff for rendering heatmap`|
|heatmap/index.html          |`Page with heatmap`|
|heatmap/resources/demands.json          |`Data used for rendering heatmap`|
|heatmap/js/main.js         |`Script with heatmap generation`|
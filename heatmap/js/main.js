const get = url => new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.open('GET', url);
    request.onload = () => {
        if (request.status === 200) {
            resolve(request.response);
        }
        else {
            reject(Error(request.statusText));
        }
    };
    request.onerror = () => reject(Error("Network Error"));
    request.send();
});

// Counts center off all points. Returns object with average latitude and longitude
const getCenter = demands => {
    const average = (array, field) => sum(array, field) / array.length;
    const sum = (array, field) => {
        const add = (accumulator, value) => accumulator + value[field];
        return array.reduce(add, 0);
    };
    const getAverageLat = demands => average(demands, 'lat');
    const getAverageLng = demands => average(demands, 'lng');
    return {lat: getAverageLat(demands), lng: getAverageLng(demands)};
};

const getMap = demands => new google.maps.Map(document.getElementById('map'), {
    zoom: 11,
    center: getCenter(demands),
    mapTypeId: 'satellite'
});

const getHeatMap = demands => new google.maps.visualization.HeatmapLayer({
    data: getPoints(demands),
    map: getMap(demands),
    radius: 22
});

const convertDemandToGooglePoint = demand => ({
    location: new google.maps.LatLng(demand.lat, demand.lng),
    weight: demand.demand
});

const getPoints = demands => demands.map(convertDemandToGooglePoint);
const renderHeatMap = response => {
    const demands = JSON.parse(response);
    getHeatMap(demands);
};

// Function called from index.html. Required for Google chart
function initMap() {
    get('/heatmap/resources/demands.json').then(renderHeatMap);
}